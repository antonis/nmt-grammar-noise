The Effect of Grammatical Errors on NMT
=============================================================

This repository contains the Spanish translations of the [JFLEG corpus](https://github.com/keisks/jfleg).

It also contains scripts, output data, and instructions
on how to produce synthetic errors for the WMT datasets.


All relevant information can be found in our [NAACL'19 paper](https://arxiv.org/pdf/1808.06267.pdf) 
"Neural Machine Translation of Text from Non-Native Speakers".

## Data

	.
	├── jfleg-es	# Translations of JFLEG in Spanish
	│   ├── dev.translations.es
	│   └── test.translations.es
	├── en-es-experiments	# The data for our English-Spanish Experiments
	│   ├── train.[errortype].en	# Training data
	│   ├── dev.[errortype].en		# Dev data
	│   └── test.[errortype].en		# Test data
	├── wmt18	# preprocessed WMT18 data with synthesized errors
	│   ├── cs-en
	│   ├── de-en
	│   ├── et-en
	│   ├── fi-en
	│   ├── ru-en
	│   ├── tr-en
	│   └── zh-en
	├── scripts		# scripts for synthesizing errors
	│   ├── create_errors.sh	# Example process of introducing all errors
	│   ├── dropone.py	# Introduce character-drop errors
	│   ├── article_errors.py	# Introduce art/det errors
	│   ├── prep_errors.py	# Introduce preposition errors
	│   ├── noun_num_errors.py	# Introduce noun number errors
	│   ├── sva_errors.py	# Introduce subject-verb agreement errors
	│   ├── find_[*].py	# Scripts that read the parses and find suitable error positions
	│   └── berkeleyparsing
	│       ├── wmt2ptb.sed		# convert WMT (moses) tokenization to PTB
	│       ├── ptb2wmt.sed		# convert PTB tokenization to WMT (moses)
	│       ├── parse_wmt.job		# Example jobscript for parsing a WMT language pair
	│       └── parse_sentences.sh		# Script to run berkeleyparser
	└── README.md



## Reference

The following paper should be cited in any publications that use this dataset:

Antonis Anastasopoulos, Alison Lui, Toan Q. Nguyen and David Chiang. [Neural Machine Translation of Text from Non-Native Speakers](https://arxiv.org/pdf/1808.06267.pdf). In NAACL-HLT, 2019.

~~~~
	@inproceedings{anastasopoulos+etal:naacl2019,
	    author = "Anastasopoulos, Antonios and Lui, Alison and Nguyen, Toan Q. and Chiang, David",
	    title = "Neural Machine Translation of Text from Non-Native Speakers",
	    booktitle = "Proc. NAACL HLT",
	    year = "2019",
	    note = "to appear"
	}
~~~~

## Prerequisites

Our error synethesizers rely on parses from the [Berkeley Parser](https://github.com/slavpetrov/berkeleyparser).
You will also need the inflection python package, and the [m2 file](http://www.comp.nus.edu.sg/~nlp/conll14st.html) from the CoNLL'14 Shared Task.

## Translation Models

The exact configurations that we used for our transformer models can be found [here](https://github.com/tnq177/nmt_text_from_non_native_speaker/blob/master/nmt/configurations.py).
A more stable version of our transformer implementation is [here](https://github.com/tnq177/witwicky).

## Questions

 - Please email Antonis Anastasopoulos (aanastas[at]andrew[dot]cmu[dot]edu).


## License

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

