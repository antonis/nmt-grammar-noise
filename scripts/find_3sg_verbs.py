#!/usr/bin/env python2.7

import sys
import StringIO
import codecs

IDX = 0


class Node(object):  # Node for n-ary tree

    def __init__(self, value, children):
        self.value = value
        self.children = children


class Tree(object):

    def __init__(self, root):
        self.root = root

    def find_nns(self, root, idx_list):
        if root.value == "VBZ":
            idx_list.append(self.get_idx(root))
        for child in root.children:
            self.find_nns(child, idx_list)

    def get_idx(self, root):
        if len(root.children):
            return self.get_idx(root.children[0])
        else:
            return root.value[1]


def peek(line):  # See next char w/o moving position
    pos = line.tell()
    char = line.read(1)
    line.seek(pos)
    return char


def parse_token(line):  # Get next token in line
    char = line.read(1)
    while char == " ":
        char = line.read(1)
    if not char:
        return None
    token = char
    if token == "(" or token == ")":
        return token
    while peek(line) != " " and peek(line) != ")" and peek(line):
        token += line.read(1)
    return token


def parse_expression(line):  # recursively build tree of operators & operands
    global IDX
    token = parse_token(line)
    if not token or token == ")":
        return None
    children = []
    if token == "(":
        token = parse_token(line)
        while peek(line) != ")" and peek(line):
            children.append(parse_expression(line))
        if peek(line) == ")":
            line.read(1)
    if not children:
        token = (token, IDX)
        IDX += 1
    return Node(token, children)


def main():
	global IDX
	c = 0
	outp = open(sys.argv[2], 'w')
	inp = codecs.open(sys.argv[1], 'r', 'utf-8').readlines()
	nolist = [int(x) for x in sys.argv[3].split('_')]
	for line in inp:
		print c
		if c in nolist:
			outp.write('\n')
		elif line.strip().count('(') != line.strip().count(')'):
			outp.write('\n')
		elif line.strip() != "( (X (SYM )) )" and line.strip() != "(())":
			IDX = 0
			line = line.strip()[2:-2]
			line = StringIO.StringIO(line)  # treat line as file
			tree = Tree(parse_expression(line))  # create tree
			idx_list = []
			try:
				tree.find_nns(tree.root, idx_list)
				outp.write(" ".join(map(str, idx_list)) + '\n')
			except:
				outp.write('\n')
				pass
		else:
			print outp.write('\n')
		c += 1
	outp.close()


if __name__ == "__main__":
    main()
