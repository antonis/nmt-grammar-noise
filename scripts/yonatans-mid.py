import numpy as np
import codecs
import sys


def mid(w):
	N = len(w)
	if N > 3:
		perm = np.random.permutation(N-2)
		first = w[0]
		last = w[-1]
		chars = [w[i+1] for i in perm]
		w = first + ''.join(chars) + last
	return w

# Read the file
with codecs.open(sys.argv[1], 'r', 'utf-8') as inp:
	lines = inp.readlines()


# Write output file
with codecs.open(sys.argv[2], 'w', 'utf-8') as outp:
	for line in lines:
		line = line.strip()
		if line:
			#swap a random character
			newline = [mid(w) for w in line.split()]
			outp.write(' '.join(newline) + '\n')
		else:
			outp.write('\n')


