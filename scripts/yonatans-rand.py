import numpy as np
import codecs
import sys
import random

def rand(w):
	return ''.join(random.sample(w,len(w)))

# Read the file
with codecs.open(sys.argv[1], 'r', 'utf-8') as inp:
	lines = inp.readlines()


# Write output file
with codecs.open(sys.argv[2], 'w', 'utf-8') as outp:
	for line in lines:
		line = line.strip()
		if line:
			#swap a random character
			newline = [rand(w) for w in line.split()]
			outp.write(' '.join(newline) + '\n')
		else:
			outp.write('\n')


