
scriptdir=/path/to/scripts
data=/path/to/wmt18/data

# Select the language you want (this is Finnish)
for l in "fi"
do
	outdir=/path/to/output/directory/wmt18/$l-en
	mkdir -p $outdir
	# Iterate over the filenames 
	for f in newstest2015 newstest2016 newstest2017 newstestB2016 newstestB2017 newsdev2015
	do
		inp=$data/$l-en/$f.tc.en
		parsed=/path/to/parses/wmt18/$l-en/parses/$f.tc.en.parsed
		mkdir -p $outdir/positions-$l
		
		# Drop one character
		outp=$outdir/$f.dropone.en
		python $scriptdir/dropone.py $inp $outp
	
		# Articles
		outp=$outdir/$f.article.en
		python $scriptdir/find_pos_ins.py $parsed $outdir/positions-$l/$f.artins -1
		python $scriptdir/article_errors.py $inp $outdir/positions-$l/$f.artins $outp
	
		# Prepositions
		outp=$outdir/$f.prep.en
		python $scriptdir/find_prep_ins.py $parsed $outdir/positions-$l/$f.prepins -1
		python $scriptdir/prep_errors.py $inp $outdir/positions-$l/$f.prepins $outp
	
		# Noun Number Errors
		outp=$outdir/$f.nounnum.en
		python $scriptdir/find_sng_nouns.py $parsed $outdir/positions-$l/$f.sngins -1
		python $scriptdir/find_pl_nouns.py $parsed $outdir/positions-$l/$f.plins -1
		python $scriptdir/noun_num_errors.py $inp $outdir/positions-$l/$f.sngins $outdir/positions-$l/$f.plins $outp
	
		# SVA
		outp=$outdir/$f.sva.en
		python $scriptdir/find_past_verbs.py $parsed $outdir/positions-$l/$f.pastins -1
		python $scriptdir/find_3sg_verbs.py $parsed $outdir/positions-$l/$f.3sgins -1
			python $scriptdir/find_n3sg_verbs.py $parsed $outdir/positions-$l/$f.n3sgins -1
		python $scriptdir/sva_errors.py $inp $outdir/positions-$l/$f.3sgins $outdir/positions-$l/$f.n3sgins $outdir/positions-$l/$f.pastins $outp

	done

done




