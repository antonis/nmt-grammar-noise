import numpy as np
import codecs
import sys


def swap(w):
	N = len(w)
	if N > 3:
		swap = np.random.randit(N-3)
		w = w[:swap+1] + w[swap+2] + w[swap+1] + w[swap+3:]
	return w

# Read the file
with codecs.open(sys.argv[1], 'r', 'utf-8') as inp:
	lines = inp.readlines()


# Write output file
with codecs.open(sys.argv[2], 'w', 'utf-8') as outp:
	for line in lines:
		line = line.strip()
		if line:
			#swap a random character
			newline = [swap(w) for w in line.split()]
			outp.write(' '.join(newline) + '\n')
		else:
			outp.write('\n')


