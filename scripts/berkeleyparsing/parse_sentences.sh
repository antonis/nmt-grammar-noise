if [ $# == 1 ]; then
	java -jar ../berkeleyparser/BerkeleyParser-1.7.jar -gr ../berkeleyparser/eng_sm6.gr < $1
elif [ $# == 2 ]; then
	java -jar ../berkeleyparser/BerkeleyParser-1.7.jar -gr ../berkeleyparser/eng_sm6.gr < $1 > $2
else
	echo "Usage: parse_sentences.sh src_file [dest_file]"
	exit 1
fi
