import numpy as np
from collections import defaultdict
import codecs
import subprocess
import sys

# Read the file
with codecs.open("/afs/crc.nd.edu/group/nlp/03/alui/scripts/conll14st-preprocessed.error-rate15.m2", 'r', 'utf-8') as inp:
	lines = inp.readlines()

preps = defaultdict(lambda:0.0)

# Parse file and collect counts
for line in lines:
	line = line.strip().split()
	if line and line[0] == 'S':
		last_sent = line[1:]
	elif line and line[0] == 'A':
		start = int(line[1])
		line2 = line[2].split('|||')
		end = int(line2[0])
		errortype = line2[1]
		subst = line2[2].lower() if line2[2] else ''
		if errortype == "Prep":
			if end == start:
				inw = ''
			elif end == start + 1:
				inw = last_sent[start].lower()
			else:
				inw = (' '.join(last_sent[start:end])).lower()
			preps[subst, inw] += 1
			

# Create confusion matrix
preplist = ['on', 'in', 'at', 'from', 'for', 'under', 'over', 'with', 'into', 'during', 'until', 'against', 'among', 'throughout','of', 'to', 'by', 'about', 'like', 'before', 'after', 'since', 'across', 'behind', 'but', 'out', 'up', 'down', 'off', '']
prepsum = defaultdict(lambda:0.0)
for a in preplist:
	for b in preplist:
		if preps[a,b] and a != b:
			prepsum[a] += preps[a,b]

prepvals = defaultdict(lambda:[])
prepps = defaultdict(lambda:[])
for d in preplist:
	for e in preplist:
		if preps[d,e] and d != e:
			prepvals[d].append(e)
			prepps[d].append(preps[d,e]/prepsum[d])

preppps = defaultdict(lambda:0.0)
allpreps = np.sum([prepsum[d] for d in prepsum])
for d in prepps:
	preppps[d] = prepsum[d]/allpreps


# Read input file (in which we will add errors)
filename2 = sys.argv[1]
with codecs.open(filename2, 'r', 'utf-8') as inp:
	lines = inp.readlines()
#lines = lines[:10]
with codecs.open(sys.argv[2], 'r', 'utf-8') as inp:
	ll = inp.readlines()
pos_ins = [[int(x) for x in l.strip().split()] for l in ll]

# Function to capitalize the first letter of a word
def capitalize(s):
	if s:
		s = s[0].upper() + s[1:]
	return s

# Write output file (with errors)
changes = 0 # this is the change counter
with codecs.open(sys.argv[3], 'w', 'utf-8') as outp:
	for idx, line in enumerate(lines):
		line = line.strip().split()
		count_occs = []
		# Count how many times each preposition exists in the line
                for d in preplist[:-1]:
			count_occs.append(line.count(d) + line.count(capitalize(d)))
                count_occs.append(len(pos_ins[idx]))
		# Multiply those counts with the probabilities if these prepositions (from the corpus)
		tempsum = np.sum([count_occs[i]*preppps[d] for i,d in enumerate(preplist)])
		# Now, if there exist some prepositions in the line
		if tempsum > 0:
			vals = preplist
			# Create probability distribution over the prepositions of the sentence
			ps = [count_occs[i] * preppps[d]/tempsum for i,d in enumerate(preplist)]
			# Randomly sample from said distribution
			ch = np.random.choice(vals, 1, p=ps)[0]

			# Now randomly sample the error, according to which preposition we chose to substitute
			subst = np.random.choice(prepvals[ch], 1, p=prepps[ch])[0]
			# Find the list of the positions where the preposition exists in the sentence (could be more than one!)
                        if ch == '':
                            pos = pos_ins[idx]
                        else:
			    pos = [index for index, word in enumerate(line) if word==ch or word==capitalize(ch)]
			# Randomly choose one of these positions (we only want to subtsitute one of them)
			ind = np.random.choice(pos, 1)[0]
			# If we are substituting (not deleting)
			if subst and ch != '':
				if line[ind][0] == line[ind][0].upper():
					subst = capitalize(subst)
				line[ind] = subst
                        # If we are inserting
                        elif subst and ch == '':
                            if ind == 0:
                                subst = capitalize(subst)
                            line.insert(ind, subst)
                            #print "Inserting", subst
                            #print ' '.join(line)
                            #print
			# Else if we are deleting
			else:
				del line[ind]
				if ind == 0:
					line[ind] = capitalize(line[ind])
			changes += 1
		# Write the changed line
		outp.write(' '.join(line) + '\n')

print "Changed: ", changes, " out of ", len(lines) , " lines."


