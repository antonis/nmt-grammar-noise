import numpy as np
from collections import defaultdict
import codecs
import subprocess
import sys

# Read the file
with codecs.open("/afs/crc.nd.edu/group/nlp/03/alui/scripts/conll14st-preprocessed.m2", 'r', 'utf-8') as inp:
	lines = inp.readlines()

dets = defaultdict(lambda:0.0)

# Parse file and collect counts
for line in lines:
	line = line.strip().split()
	if line and line[0] == 'S':
		last_sent = line[1:]
	elif line and line[0] == 'A':
		start = int(line[1])
		line2 = line[2].split('|||')
		end = int(line2[0])
		errortype = line2[1]
		subst = line2[2].lower() if line2[2] else ''
		if errortype == "ArtOrDet":
			if end == start:
				inw = ''
			elif end == start + 1:
				inw = last_sent[start].lower()
			else:
				inw = (' '.join(last_sent[start:end])).lower()
			dets[subst, inw] += 1

# Create confusion matrix (not really a matrix, everything stored in lexicons)
detlist = ['a', 'an', 'the', '']
detsum = defaultdict(lambda:0.0)
for d in detlist:
	for e in detlist:
		if e != d:
			detsum[d] += dets[d,e]

detvals = defaultdict(lambda:[])
detps = defaultdict(lambda:[])
for d in detlist:
	for e in detlist:
		if dets[d,e] and d != e:
			detvals[d].append(e)
			detps[d].append(dets[d,e]/detsum[d])

detpps = {}
alldets = np.sum([detsum[d] for d in detsum])
for d in detps:
	detpps[d] = detsum[d]/alldets

# Print it
#print "******"
#for d in detvals:
#	print d, " : ", detpps[d]
#	print '\t'.join(detvals[d])
#	print '\t'.join(str(int(item*100)/100.0) for item in detps[d])

# Read input file (in which we will add errors)
filename2 = sys.argv[1]
with codecs.open(filename2, 'r', 'utf-8') as inp:
	lines = inp.readlines()
#lines = lines[:10]
with codecs.open(sys.argv[2], 'r', 'utf-8') as inp:
	ll = inp.readlines()
pos_ins = [[int(x) for x in l.strip().split()] for l in ll]
#pos_ins = [map(int, line.split()) for line in subprocess.check_output("cat "+filename+" | ./find_pos_ins.py", shell=True).splitlines()]

print "Read all files ok"

# Function to capitalize the first letter of a word
def capitalize(s):
	if s:
		s = s[0].upper() + s[1:]
	return s

# Write output file (with errors)
changes = 0 # this is the change counter
with codecs.open(sys.argv[3], 'w', 'utf-8') as outp:
	for idx, line in enumerate(lines):
                line = line.strip().split()
		# Count existing article occurrences (Easy in the article case)
		ays = line.count('a') + line.count('A')
		ans = line.count('an') + line.count('An')
		thes = line.count('the') + line.count('The')
                # Count possible insertions
                pos_ins_count = len(pos_ins[idx])

		# If there are some possible article errors
                if ays + ans + thes + pos_ins_count:
			# Convert the counts to probabilities and 
			tempsum = float(ays*detpps['a'] + ans*detpps['an'] + thes*detpps['the'] + pos_ins_count*detpps[''])
			vals = ['a', 'an', 'the', '']
			# ... make a probability distribution
			ps = [ays*detpps['a']/tempsum, ans*detpps['an']/tempsum, thes*detpps['the']/tempsum, pos_ins_count*detpps['']/tempsum]
			# ... and sample from it
			ch = np.random.choice(vals, 1, p=ps)[0]
			
			# Sample an error
			subst = np.random.choice(detvals[ch], 1, p=detps[ch])[0]
			# Sample a position, in case the same article exists in more than one positions
                        if ch == '':
                            pos = pos_ins[idx]
                        else:
			    pos = [index for index, word in enumerate(line) if word == ch or word == capitalize(ch)]
			ind = np.random.choice(pos, 1)[0]
			# If we are substituting
			if subst and ch != '':
			    if line[ind][0] == line[ind][0].upper():
			        subst = capitalize(subst)
			    line[ind] = subst
                        # If we are inserting
                        elif subst and ch == '':
                            if ind == 0:
                                subst = capitalize(subst)
                            line.insert(ind, subst)
			# Else if we are deleting an article
			else:
				del line[ind]
				if ind == 0:
					line[ind] = capitalize(line[ind])
			changes += 1
		# Write sentence with errors
		outp.write(' '.join(line) + '\n')

print "Changed: ", changes, " out of ", len(lines) , " lines."





