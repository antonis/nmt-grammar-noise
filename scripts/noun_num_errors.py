import numpy as np
from collections import defaultdict
import codecs
import subprocess
import inflection
import sys

# Read the file
with codecs.open("/afs/crc.nd.edu/group/nlp/03/alui/scripts/conll14st-preprocessed.error-rate15.m2", 'r', 'utf-8') as inp:
	lines = inp.readlines()

false_plurals = 0.0
false_singulars = 0.0

# Parse file and collect counts
for line in lines:
	line = line.strip().split()
	if line and line[0] == 'S':
		last_sent = line[1:]
	elif line and line[0] == 'A':
		start = int(line[1])
		line2 = line[2].split('|||')
		end = int(line2[0])
		errortype = line2[1]
		subst = line2[2].lower() if line2[2] else ''
		if errortype == "Nn":
			if end == start + 1:
			    inw = last_sent[start].lower()
			else: #  Not sure how to deal
                            continue
                        if inflection.pluralize(inw) == inw:
                            false_plurals += 1
                        else:
                            false_singulars += 1

false_plural_prob = false_plurals/(false_plurals+false_singulars)
false_singular_prob = false_singulars/(false_plurals+false_singulars)

# Print it
print "******"
print "Incorrect plural:", false_plural_prob
print "Incorrect singular:", false_singular_prob
print "******"

# Read input file (in which we will add errors)
filename2 = sys.argv[1]
with codecs.open(filename2, 'r', 'utf-8') as inp:
	lines = inp.readlines()
#lines = lines[:10]
with codecs.open(sys.argv[2], 'r', 'utf-8') as inp:
	ll = inp.readlines()
sng_nouns = [[int(x) for x in l.strip().split()] for l in ll]
with codecs.open(sys.argv[3], 'r', 'utf-8') as inp:
	ll = inp.readlines()
pl_nouns = [[int(x) for x in l.strip().split()] for l in ll]

# Read input file (in which we will add errors)
#filename = "test50.en"
#with codecs.open(filename, 'r', 'utf-8') as inp:
#	lines = inp.readlines()
#subprocess.call("./parse_sentences.sh "+filename+" parsed.tmp", shell=True)
#pl_nouns = [map(int, line.split()) for line in subprocess.check_output("cat parsed.tmp | ./find_pl_nouns.py", shell=True).splitlines()]
#sng_nouns = [map(int, line.split()) for line in subprocess.check_output("cat parsed.tmp | ./find_sng_nouns.py", shell=True).splitlines()]
#subprocess.call("rm ./parsed.tmp", shell=True)

punctuation = [".", ",", ":", "?", "!", "\"", ";", "\'"]

def all_punct(a):
	for c in a:
		if c not in punctuation:
			return True
	return False

# Write output file (with errors)
changes = 0 # this is the change counter
with codecs.open(sys.argv[4], 'w', 'utf-8') as outp:
	for idx, line in enumerate(lines):
                line = line.strip().split()
		# Count nouns
		npl = len(pl_nouns[idx])
		nsng = len(sng_nouns[idx])

		# If there are some possible noun num errors
		if npl + nsng:
			# Convert the counts to probabilities and 
			tempsum = float(npl*false_singular_prob + nsng*false_plural_prob)
			is_pl_vals = [1, 0]
			# ... make a probability distribution
			ps = [npl*false_singular_prob/tempsum, nsng*false_plural_prob/tempsum]
			# ... and sample from it
			pl = np.random.choice(is_pl_vals, 1, p=ps)[0]
			
			# Sample a position, in case their are multiple sing/pl nouns
			if pl:
				pos = pl_nouns[idx]
			else:
				pos = sng_nouns[idx]
			ind = np.random.choice(pos, 1)[0]
			#print line, ind
			not_all_punct = all_punct(line[ind])
			if line[ind] and not_all_punct:
				add = ""
				try:
					print line[ind]
				except:
					pass
				while line[ind][-1] in punctuation:
					add = line[ind][-1] + add
					line[ind] = line[ind][:-1]
				if pl:
					line[ind] = inflection.singularize(line[ind])
				else:
					line[ind] = inflection.pluralize(line[ind])
				line[ind] = line[ind]+add
				changes += 1
		# Write sentence with errors
		outp.write(' '.join(line) + '\n')

print "Changed: ", changes, " out of ", len(lines) , " lines."
