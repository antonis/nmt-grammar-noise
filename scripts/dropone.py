import numpy as np
import codecs
import sys


# Read the file
with codecs.open(sys.argv[1], 'r', 'utf-8') as inp:
	lines = inp.readlines()


# Write output file
with codecs.open(sys.argv[2], 'w', 'utf-8') as outp:
	for line in lines:
		line = line.strip()
		if line:
			N = len(line)
			#drop a random character
			drop = np.random.randint(N)
			line = line[:drop] + line[drop+1:]
			outp.write(line + '\n')
		else:
			outp.write('\n')


