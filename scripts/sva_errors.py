import numpy as np
from collections import defaultdict
import codecs
import subprocess
import inflection
from pattern.en import tenses, conjugate
import sys

# Read the file
with codecs.open("/afs/crc.nd.edu/group/nlp/03/alui/scripts/conll14st-preprocessed.error-rate15.m2", 'r', 'utf-8') as inp:
	lines = inp.readlines()

false_3sg = 0.0
false_n3sg = 0.0
false_2sgp = 0.0
false_n2sgp = 0.0
false_plurals = 0.0
false_singulars = 0.0

# Parse file and collect counts
for line in lines:
	line = line.strip().split()
	if line and line[0] == 'S':
		last_sent = line[1:]
	elif line and line[0] == 'A':
		start = int(line[1])
		line2 = line[2].split('|||')
		end = int(line2[0])
		errortype = line2[1]
		subst = line2[2].lower() if line2[2] else ''
		if errortype == "SVA":
			if end == start + 1:
			    inw = last_sent[start].lower()
			else: #  Not sure how to deal
				continue
			if '3sg' in tenses(subst):
				false_n3sg += 1
			elif '2sgp' in tenses(subst):
				false_n2sgp += 1
			elif ('1sg' in tenses(subst) or '2sg' in tenses(subst)):
				false_3sg += 1
			elif ('1sgp' in tenses(subst) or '3sgp' in tenses(subst)):
				false_2sgp += 1

false_3sg_prob = false_3sg/(false_3sg+false_n2sgp+false_2sgp+false_n3sg)
false_n3sg_prob = false_n3sg/(false_3sg+false_n2sgp+false_2sgp+false_n3sg)
false_2sgp_prob = false_2sgp/(false_3sg+false_n2sgp+false_2sgp+false_n3sg)
false_n2sgp_prob = false_n2sgp/(false_3sg+false_n2sgp+false_2sgp+false_n3sg)
false_past_prob = (false_n2sgp + false_2sgp)/(false_3sg+false_n2sgp+false_2sgp+false_n3sg)
false_present_prob = (false_n3sg + false_3sg)/(false_3sg+false_n2sgp+false_2sgp+false_n3sg)
#false_plural_prob = false_plurals/(false_plurals+false_singulars)
#false_singular_prob = false_singulars/(false_plurals+false_singulars)

# Print it
print "******"
print "Incorrect 3sg:", false_3sg_prob
print "Incorrect n3sg:", false_n3sg_prob
print "Incorrect 2sgp:", false_2sgp_prob
print "Incorrect n2sgp:", false_n2sgp_prob
#print "Incorrect plural:", false_plural_prob
#print "Incorrect singular:", false_singular_prob
print "******"

# Read input file (in which we will add errors)
filename2 = sys.argv[1]
with codecs.open(filename2, 'r', 'utf-8') as inp:
	lines = inp.readlines()
#lines = lines[:10]

# TODO: Read the first three from files
with codecs.open(sys.argv[2], 'r', 'utf-8') as inp:
	ll = inp.readlines()
pos_3sg_verbs = [[int(x) for x in l.strip().split()] for l in ll]
with codecs.open(sys.argv[3], 'r', 'utf-8') as inp:
	ll = inp.readlines()
pos_n3sg_verbs = [[int(x) for x in l.strip().split()] for l in ll]
with codecs.open(sys.argv[4], 'r', 'utf-8') as inp:
	ll = inp.readlines()
pos_past_verbs = [[int(x) for x in l.strip().split()] for l in ll]
pos_2sgp_verbs = []
pos_n2sgp_verbs = []

#pl_verbs = [map(int, line.split()) for line in subprocess.check_output("cat parsed.tmp | ./find_pl_nouns.py", shell=True).splitlines()]
#sng_verbs = [map(int, line.split()) for line in subprocess.check_output("cat parsed.tmp | ./find_sng_nouns.py", shell=True).splitlines()]
#subprocess.call("rm ./parsed.tmp", shell=True)


for idx, line in enumerate(lines):
	line = line.strip().split()
	pos_2sgp_verbs.append([])
	pos_n2sgp_verbs.append([])
	if pos_past_verbs[idx]:
		for j in pos_past_verbs[idx]:
			if '2sgp' in tenses(line[j]):
				pos_2sgp_verbs[-1].append(j)
			else:
				pos_n2sgp_verbs[-1].append(j)

punctuation = [".", ",", ":", "?", "!", "\"", ";"]

# Write output file (with errors)
changes = 0 # this is the change counter
with codecs.open(sys.argv[5], 'w', 'utf-8') as outp:
	for idx, line in enumerate(lines):
		line = line.strip().split()
		# Count verbs
		n_3sg = len(pos_3sg_verbs[idx])
		n_n3sg = len(pos_n3sg_verbs[idx])
		n_2sgp = len(pos_2sgp_verbs[idx])
		n_n2sgp = len(pos_n2sgp_verbs[idx])
		#npl = len(pl_verbs[idx])
		#nsng = len(sng_verbs[idx])
		# If there are some possible noun num errors
		if n_3sg + n_n3sg + n_2sgp + n_n2sgp:
			# Convert the counts to probabilities and 
			tempsum = float(n_3sg*false_3sg_prob + n_n3sg*false_n3sg_prob + n_2sgp*false_2sgp_prob + n_n2sgp*false_n2sgp_prob)
			is_pl_vals = [0,1,2,3]
			# ... make a probability distribution
			ps = [n_3sg*false_3sg_prob/tempsum, n_n3sg*false_n3sg_prob/tempsum, n_2sgp*false_2sgp_prob/tempsum, n_n2sgp*false_n2sgp_prob/tempsum]
			#ps = [npl*false_singular_prob/tempsum, nsng*false_plural_prob/tempsum]
			# ... and sample from it
			pl = np.random.choice(is_pl_vals, 1, p=ps)[0]
			
			# Sample a position, in case their are multiple sing/pl nouns
			if pl == 0:
				pos = pos_3sg_verbs[idx]
			elif pl == 1:
				pos = pos_n3sg_verbs[idx]
			elif pl == 2:
				pos = pos_2sgp_verbs[idx]
			elif pl == 3:
				pos = pos_n2sgp_verbs[idx]
			ind = np.random.choice(pos, 1)[0]
			print line
			print pl, pos, ind

			if len(line[ind])>1:
				add = ""
				backup = line[ind]
				while line[ind] and line[ind][-1] in punctuation:
					add = line[ind][-1] + add
					line[ind] = line[ind][:-1]
				if pl == 0:
					line[ind] = conjugate(line[ind], '1sg', parse=True)
				elif pl == 1:
					line[ind] = conjugate(line[ind], '3sg', parse=True)
				elif pl == 2:
					line[ind] = conjugate(line[ind], '1sgp', parse=True)
				elif pl == 3:
					line[ind] = conjugate(line[ind], '2sgp', parse=True)
				if line[ind]:
					line[ind] = line[ind]+add
				else:
					line[ind] = backup
					changes -= 1
				changes += 1
		# Write sentence with errors
		outp.write(' '.join(line) + '\n')

print "Changed: ", changes, " out of ", len(lines) , " lines."
